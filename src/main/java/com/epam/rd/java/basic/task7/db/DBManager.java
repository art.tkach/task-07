package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static final String DATABASE_URL;
	private static final Properties properties = new Properties();

	static {
		try {
			properties.load(new FileReader("app.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		DATABASE_URL = (String) properties.get("connection.url");
	}

	public static synchronized DBManager getInstance(){
		if (instance==null){
			try{
				instance = new DBManager();
			}
			catch (Exception ex){
				ex.printStackTrace();
			}

		}
		return instance;
	}

	private DBManager() {

	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		String query = "select u.id, u.login from users u order by u.id";
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				User user = new User();
				user.setLogin(rs.getString("login"));
				user.setId(rs.getInt("ID"));
				users.add(user);
			}
		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		boolean insertSuccess = false;
		String query = "insert into users(login) values(?)";

		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement stmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS)){
			stmt.setString(1, user.getLogin());
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()){
				user.setId(rs.getInt(1));
			}
			insertSuccess = true;
		}
		catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return insertSuccess;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users.length==0) return false;


		StringJoiner joiner = new StringJoiner(",");
		//Готовлю массив
		for (User user : users) {
			joiner.add(String.valueOf(user.getId()));
		}
		String query =  String.format("delete from users where id in(%s)", joiner.toString());

		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){

			pstmt.executeUpdate();
		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}

		return true;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		String query = "select u.id, u.login from users u where u.login = ? order by id";
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){
			pstmt.setString(1, login);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				user = User.createUser(login);
				user.setId(rs.getInt(1));
				break;
			}

		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		String query = "select t.id, t.name from teams t where t.name = ? order by id";
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){
				pstmt.setString(1, name);
				ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				team = Team.createTeam(name);
				team.setId(rs.getInt(1));
				break;
			}

		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String query = "select t.id, t.name from teams t order by t.id";
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				Team team = new Team();
				team.setName(rs.getString("name"));
				team.setId(rs.getInt("ID"));
				teams.add(team);
			}
		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {

		boolean insertSuccess = false;
		String query = "insert into teams(name) values(?)";
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement stmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
		){
			stmt.setString(1, team.getName());
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()){
				team.setId(rs.getInt(1));
			}
			insertSuccess = true;
		}
		catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}

		return insertSuccess;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		//region Validation
		if (user==null) return false;
		for (Team t:teams) {
			if (t==null) return false;
		}
		//endregion
		boolean insertSuccess = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		String query = "insert into users_teams(user_id, team_id) values(?,?)";

		try{
			con = getConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			for (Team t:teams) {
				pstmt.setInt(1,user.getId());
				pstmt.setInt(2,t.getId());
				pstmt.executeUpdate();
			}
			con.commit();
			insertSuccess = true;
		}
		catch (SQLException ex){
			rollback(con);
			throw new DBException();
		}
		finally {
			close(con, pstmt, null);
		}
		return insertSuccess;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams= new ArrayList<>();
		String query = "select team_id from users_teams where user_id = ?";

		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){
			pstmt.setInt(1, user.getId());
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				teams.add(getTeamById(rs.getInt(1)));
			}
			rs.close();
		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String queryDeleteUsersTeams = "delete from users_teams where team_id = " + team.getId();
		String queryDeleteTeam = "delete from teams where id = "+ team.getId();
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			Statement stmt = con.createStatement()){
			stmt.addBatch(queryDeleteTeam);
			stmt.addBatch(queryDeleteUsersTeams);
			stmt.executeBatch();
		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = "update teams t set t.name = ? where t.id = ?";
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){
			pstmt.setString(1,team.getName());
			pstmt.setInt(2,team.getId());
			pstmt.executeUpdate();
		}catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}

		return true;
	}

	private Team getTeamById(int id){
		Team team = null;
		String query = "select t.id, t.name from teams t where t.id = ? order by id";
		try(Connection con = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement pstmt = con.prepareStatement(query)){
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				team = Team.createTeam(rs.getString(2));
				team.setId(rs.getInt(1));
				break;
			}

		}catch (SQLException ex){
			ex.printStackTrace();
		}
		return team;
	}

	private Connection getConnection() throws DBException{
		Connection con = null;
		try{
			con = DriverManager.getConnection(DATABASE_URL);
		}
		catch (SQLException ex){
			throw new DBException();
		}
		return con;
	}

	private void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
	private void close(Connection con, PreparedStatement pstmt, ResultSet rs){
		close(con);
		close(pstmt);
		close(rs);
	}

	/**
	 * closes ResultSet
	 * @param rs ResultSet
	 */
	private void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * closes Statament
	 * @param st Statement
	 */
	private void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * closes Connection
	 * @param con Connection
	 */

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
